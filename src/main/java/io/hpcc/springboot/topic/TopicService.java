package io.hpcc.springboot.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService {

	@Autowired
	private TopicRepositry topicRepo;

	public List<Topic> getAllTopics() {
		List<Topic> topics = new ArrayList<>();
		topicRepo.findAll().forEach(topics::add);
		/*for (Topic topic : topicRepo.findAll()) {
	       topics.add(topic);
		}*/
		return topics;
		
		
	}

	public Topic getTopicID(String id) {
		return topicRepo.findOne(id);
	}

	public void addTopic(Topic topic) {
		//System.out.println(topic);
		topicRepo.save(topic);

	}

	public void updateTopic(String id, Topic topic) {
		topicRepo.save(topic);
	}

	public void deleteTopic(String id) {
		topicRepo.delete(id);

	}
}
